package main

import (
	"go-example-crud/api/configs"
	"go-example-crud/api/routes"

	"github.com/joho/godotenv"
)

// @description Api Documentation
// @version 0.0.1

// @schemes http https
// @securityDefinitions.apiKey authtoken
// @in header
// @name authorization

// @tag.name auth
// @tag.description list of auth api
func main() {
	godotenv.Load()
	configs.GetConfig()
	routes.CreateHandler()
}
