package dto

import "go.mongodb.org/mongo-driver/bson/primitive"

type UserCreateRequest struct {
	Name string `json:"name" example:"eko"`
}

type UserCreateResponse struct {
	ID primitive.ObjectID `json:"_id" example:"123"`
}

type UserEditRequest struct {
	Name string `json:"name" example:"eko"`
}

type UserEditResponse struct {
	ID primitive.ObjectID `json:"_id" example:"123"`
}

type UserByIDResponse struct {
	Response userByIDValue `json:"response"`
}

type userByIDValue struct {
	ID   primitive.ObjectID `json:"_id" example:"123"`
	Name string             `json:"name" example:"eko"`
}
