package configs

import (
	"fmt"
	"os"
)

type config struct {
	Environment *string
	Server      *serverConfig
	DB          *dbConfig
}

type serverConfig struct {
	Hostname string `default:"localhost"`
	Port     string `default:"5000"`
	Cors     string `default:"*"`
	BaseURL  string
}

type dbConfig struct {
	Host string
	Name string
}

var ConfigInstance *config

func GetConfig() {
	if ConfigInstance == nil {
		server := getServerConfig()
		db := getDBConfig()

		ConfigInstance = &config{
			Server: &server,
			DB:     &db,
		}
		fmt.Println("Get Config Instance.")
	}
}

func getServerConfig() serverConfig {
	port := os.Getenv("SERVER_PORT")
	if port == "" {
		port = "5000"
	}

	baseURL := os.Getenv("BASE_URL")
	if baseURL == "" {
		baseURL = "localhost:5000"
	}

	server := serverConfig{
		Port:    port,
		BaseURL: baseURL,
	}

	return server
}

func getDBConfig() dbConfig {
	host := os.Getenv("DB_HOST")
	if host == "" {
		host = "mongodb://localhost:27017"
	}

	name := os.Getenv("DB_NAME")
	if name == "" {
		name = "goExampleCrudDB"
	}

	db := dbConfig{
		Host: host,
		Name: name,
	}

	return db
}
