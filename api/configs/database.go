package configs

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Resource struct {
	DB *mongo.Database
}

func InitDB() (*Resource, error) {
	host := ConfigInstance.DB.Host
	dbName := ConfigInstance.DB.Name
	mongoClient, err := mongo.NewClient(options.Client().ApplyURI(host))
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err = mongoClient.Connect(ctx)
	if err != nil {
		return nil, err
	}

	fmt.Println("Mongo connected at :", host)

	return &Resource{DB: mongoClient.Database(dbName)}, nil
}
