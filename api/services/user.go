package services

import (
	"go-example-crud/api/configs"
	"go-example-crud/api/models"
	"go-example-crud/api/utils"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var userRepo *mongo.Collection

func InitUserResource(resource *configs.Resource) {
	userRepo = resource.DB.Collection("users")
}

func CreateUser(user *models.User) (result *mongo.InsertOneResult, err error) {
	ctx, cancel := utils.InitContext()
	defer cancel()
	result, err = userRepo.InsertOne(ctx, user)
	if err != nil {
		return
	}

	return
}

func DeleteUser(id *string) (err error) {
	ctx, cancel := utils.InitContext()
	defer cancel()
	objectId, err := primitive.ObjectIDFromHex(*id)
	if err != nil {
		return
	}

	filter := bson.M{
		"_id": objectId,
	}

	userRepo.DeleteOne(ctx, filter)

	if err != nil {
		return
	}

	return
}

func EditUser(id *string, user *models.User) (err error) {
	ctx, cancel := utils.InitContext()
	defer cancel()
	objectId, err := primitive.ObjectIDFromHex(*id)
	if err != nil {
		return
	}
	userRepo.UpdateByID(ctx, objectId, bson.M{"$set": user})

	if err != nil {
		return
	}

	return
}

func FindUserByID(id *string) (*models.User, error) {
	ctx, cancel := utils.InitContext()
	defer cancel()

	objectId, err := primitive.ObjectIDFromHex(*id)
	if err != nil {
		return nil, err
	}

	result := userRepo.FindOne(ctx, bson.M{"_id": objectId})
	var user models.User
	result.Decode(&user)

	return &user, nil
}

func FindUserByName(name *string) (*models.User, error) {
	ctx, cancel := utils.InitContext()
	defer cancel()

	result := userRepo.FindOne(ctx, bson.M{"name": name})
	var user models.User
	result.Decode(&user)

	return &user, nil
}

func FindAllUser() *mongo.Cursor {
	ctx, cancel := utils.InitContext()
	defer cancel()

	result, err := userRepo.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}

	return result
}
