package controllers

import (
	"go-example-crud/api/dto"
	"go-example-crud/api/services"
	"net/http"

	"github.com/gin-gonic/gin"
)

type AuthController struct{}

// @Summary Login
// @Accept  json
// @Tags auth
// @Produce  json
// @Param   body     body    dto.UserLoginRequest     true        "Request body for login"
// @Success 200 {object} dto.UserLoginResponse "Successful operation"
// @Router /auth/login [post]
// @Security authtoken
func (u *AuthController) Login(ctx *gin.Context) {
	requestedUser := new(dto.UserLoginRequest)
	ctx.BindJSON(requestedUser)
	result, err := services.FindUserByName(&requestedUser.Name)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	if result.Password != requestedUser.Password {
		ctx.JSON(http.StatusBadRequest, gin.H{"response": "password salah"})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"response": "login OK"})
}
