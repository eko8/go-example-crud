package controllers

import (
	"go-example-crud/api/dto"
	"go-example-crud/api/models"
	"go-example-crud/api/services"
	"net/http"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type UserController struct{}

func (u *UserController) FindAllUser(ctx *gin.Context) {
	var users []models.User
	query := services.FindAllUser()
	query.All(ctx, &users)
	ctx.JSON(http.StatusOK, gin.H{"result": users})
}

func (u *UserController) CreateUser(ctx *gin.Context) {
	user := new(dto.UserCreateRequest)
	err := ctx.ShouldBindJSON(user)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userModel := new(models.User)
	userModel.Name = user.Name

	result, err := services.CreateUser(userModel)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	response := new(dto.UserCreateResponse)
	response.ID = result.InsertedID.(primitive.ObjectID)

	ctx.JSON(http.StatusOK, response)
}

func (u *UserController) FindUserByID(ctx *gin.Context) {
	id := ctx.Param("userId")

	result, err := services.FindUserByID(&id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	var response dto.UserByIDResponse
	response.Response.ID = result.ID
	response.Response.Name = result.Name
	ctx.JSON(http.StatusOK, response)
}

func (u *UserController) DeleteUser(ctx *gin.Context) {
	id := ctx.Param("userId")
	user, err := services.FindUserByID(&id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	if user.ID.IsZero() {
		ctx.String(http.StatusBadRequest, "User Not Found")
		return
	}

	err = services.DeleteUser(&id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"result": "update ok"})

}

func (u *UserController) EditUser(ctx *gin.Context) {
	id := ctx.Param("userId")
	user, err := services.FindUserByID(&id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	if user.ID.IsZero() {
		ctx.String(http.StatusBadRequest, "User Not Found")
		return
	}

	requestedUser := new(dto.UserEditRequest)
	err = ctx.ShouldBindJSON(requestedUser)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user.Name = requestedUser.Name

	err = services.EditUser(&id, user)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"result": "update ok"})
}
