package routes

import (
	"go-example-crud/api/configs"
	docs "go-example-crud/docs"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func CreateHandler() {
	gin.SetMode(gin.ReleaseMode)

	router := gin.New()
	router.Use(CORSMiddleware())
	// router.Use(gin.Logger())

	docs.SwaggerInfo.BasePath = "/api"
	docs.SwaggerInfo.Title = "Server"

	publicRoute := router.Group("/api")
	resource, err := configs.InitDB()
	if err != nil {
		log.Fatal(err)
	}

	InitAuthRoute(publicRoute, resource)
	InitUserRoute(publicRoute, resource)

	//swagger Route
	publicRoute.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// health check
	router.GET("/api/health-check/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "PONG!")
	})

	router.Run(":" + configs.ConfigInstance.Server.Port)

}
