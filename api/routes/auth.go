package routes

import (
	"go-example-crud/api/configs"
	"go-example-crud/api/controllers"

	"github.com/gin-gonic/gin"
)

func InitAuthRoute(app *gin.RouterGroup, resource *configs.Resource) {
	authRoute := app.Group("/auth")
	authController := new(controllers.AuthController)
	authRoute.POST("/login", authController.Login)
}
