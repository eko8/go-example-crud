package routes

import (
	"go-example-crud/api/configs"
	"go-example-crud/api/controllers"
	"go-example-crud/api/services"

	"github.com/gin-gonic/gin"
)

func InitUserRoute(app *gin.RouterGroup, resource *configs.Resource) {
	services.InitUserResource(resource)
	userRoute := app.Group("/user")
	userController := new(controllers.UserController)
	userRoute.POST("/create", userController.CreateUser)
	userRoute.PUT("/edit/:userId", userController.EditUser)
	userRoute.DELETE("/delete/:userId", userController.DeleteUser)
	userRoute.GET("/:userId", userController.FindUserByID)
	userRoute.GET("/all", userController.FindAllUser)
}
